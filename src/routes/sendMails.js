const express = require('express');
const logger = require('../utils/logger');
const emailValidator = require('../validation-schemas/email');
const mailer = require('../utils/mailer')
const jwt = require('jsonwebtoken');

const router = express.Router();

router.post('/highestScore', (req, res, next) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        }else {
            const email = req.body.email;
            const count = req.body.count;
            const valid = emailValidator.validate({email, count});
            if (valid.error == null) {
                mailer.sendMail(email, count)
                    .then(() => {
                        res.json({msg: 'Mail Sent'});
                    })
                    .catch(() => {
                        logger.error('Error sending mail using mailgun! Please try again');
                        res.status(500).json({msg: 'Error sending mail using mailgun! Please try again'});
                    });
            } else {
                res.status(403).json({msg: 'Bad request, entries cannot be null or enter a valid email'});
            }
        }
    });
})

module.exports = router;
