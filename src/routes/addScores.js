const express = require('express');
const connection = require('../config/config');
const logger = require('../utils/logger');
const addScoreValidator = require('../validation-schemas/add-score');
const jwt = require('jsonwebtoken');

const router = express.Router();

router.post('/' , (req, res, next) => {
    jwt.verify(req.token, 'secretkey' , (err , authData) => {
        if (err) {
            res.sendStatus(403);
        }else {
            const name = req.body.playerName;
            const level = req.body.level;
            const bestScore = req.body.bestScore;
            const valid = addScoreValidator.validate({name,level,bestScore})
            if (valid.error == null) {
                const query = `INSERT INTO ScoreList (playerName , level , bestScore) VALUES (${connection.escape(name)} , ${connection.escape(level)} , ${connection.escape(bestScore)});`
                connection.query(query , (err,data) => {
                    if(err) {
                        logger.error(err);
                        res.status(500).json({msg: 'Some internal error'});
                    }else {
                        res.json({msg: 'Score Added'});
                    }
                })
            }else {
                res.status(403).json({msg: 'Bad request, entries cannot be null'});
            }
        }
    })
})

module.exports = router;
