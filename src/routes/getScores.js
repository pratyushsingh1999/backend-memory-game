const express = require('express');
const connection = require('../config/config');
const logger = require('../utils/logger');

const router = express.Router();

router.get('/easy' , (req, res, next) => {
    connection.query('SELECT * FROM ScoreList\n' +
        'WHERE level=\'easy\'\n' +
        'ORDER BY bestScore ASC;' , (err, data) => {
        if (err) {
            logger.error(err);
            res.status(500).json({msg: 'Some Internal Error'});
        }
        res.json(data);
    });
})
router.get('/medium' , (req, res, next) => {
    connection.query('SELECT * FROM ScoreList\n' +
        'WHERE level=\'medium\'\n' +
        'ORDER BY bestScore ASC;' , (err, data) => {
        if (err) {
            logger.error(err);
            res.status(500).json({msg: 'Some Internal Error'});
        }
        res.json(data);
    });
})
router.get('/hard' , (req, res, next) => {
    connection.query('SELECT * FROM ScoreList\n' +
        'WHERE level=\'hard\'\n' +
        'ORDER BY bestScore ASC;' , (err, data) => {
        if (err) {
            logger.error(err);
            res.status(500).json({msg: 'Some Internal Error'});
        }
        res.json(data);
    });
})

module.exports = router;
