const express = require('express');
const connection = require('../config/config');
const logger = require('../utils/logger');
const jwt = require('jsonwebtoken');

const router = express.Router();

router.get('/' , (req, res, next) => {
    jwt.verify(req.token, 'secretkey' , (err, user) => {
        res.json(user);
    })
})

module.exports = router;
