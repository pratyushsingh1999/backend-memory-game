const express = require('express');
const connection = require('../config/config');
const logger = require('../utils/logger');
const bcrypt = require('bcrypt');
const loginValidator = require('../validation-schemas/login')
const signupValidator = require('../validation-schemas/singup')
const jwt = require('jsonwebtoken');
const mailer = require('../utils/mailer');

const router = express.Router();

router.get('/verifyEmail' , (req, res, next) => {
    const email = req.query.email;
    connection.query(`UPDATE UserList SET verified = true WHERE email=${connection.escape(email)}` , (err , data)=> {
        if (err) {
            req.status(400).json({msg: 'Not verified'});
        }else {
            res.json({msg: 'Verified'});
        }
    })
})

router.post('/login', (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    const valid = loginValidator.validate({email, password});
    if (valid.error == null) {
        connection.query(`SELECT * FROM UserList WHERE email = ${connection.escape(email)}`, (err, data) => {
            if (err || data.length<1) {
                res.status(400).json({msg: 'User not found'});
                return;
            }
            bcrypt.compare(password, data[0]['password'])
                .then((match) => {
                    if (match) {
                        if (data[0].verified) {
                            jwt.sign({user: data[0]} , 'secretkey' , (err, token) => {
                                return res.json({token , user: data[0]});
                            })
                        }else {
                            return res.status(400).json({msg: 'Please verify email'})
                        }
                    }else {
                        res.status(400).json({msg: 'Wrong password'})
                    }
                })
                .catch((err) => {
                    logger.error(err);
                    res.status(500).json({msg: 'Some internal error'})
                })
        })
    } else {
        res.status(403).json({msg: 'Bad request'});
    }
})
router.post('/signup', (req, res, next) => {
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    const valid = signupValidator.validate({name, email, password});
    if (valid.error == null) {
        bcrypt.hash(password, 6)
            .then((data) => {
                connection.query(`INSERT INTO UserList (name, email, password, verified) 
                                                  VALUES (${connection.escape(name)} , ${connection.escape(email)} , ${connection.escape(data)} , false)`, (err, data) => {
                    if (err) {
                        logger.error(err);

                    } else {
                        mailer.sendVerificationMail(email)
                            .then((data) => {
                                logger.info(data);
                            })
                            .catch((err) => {
                            logger.error(err);
                        });
                        res.json({msg: "User registered successfully, please verify email"})
                    }
                })
            })
            .catch((err) => {
                logger.error(err);
            })
        // res.json({msg: 'Good request'});
    } else {
        res.status(403).json({msg: 'Bad request'});
    }
})

module.exports = router;
