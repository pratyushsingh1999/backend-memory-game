const express = require('express');
const app = express();
const cors = require('cors');
require("dotenv").config();
const PORT = process.env.PORT || 8000;
const logger = require('./utils/logger');
const bodyParser = require('body-parser');

const getScoresRoute = require('./routes/getScores');
const addScoresRoute = require('./routes/addScores');
const sendMailsRoute = require('./routes/sendMails');
const authRoutes = require('./routes/auth');
const getUserByTokenRoute = require('./routes/getUserByToken')

app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(cors());

app.use((req, res, next) => {
    logger.info(req.url);
    next();
})

app.use('/api', authRoutes);
app.use('/api/getAllScores' , getScoresRoute);
app.use('/api/addScore' , verifyToken , addScoresRoute);
app.use('/api/sendMail', verifyToken , sendMailsRoute);
app.use('/api/getUserByToken', verifyToken , getUserByTokenRoute);


app.use((req, res, next) => {
    res.status(404).send('Error page not found');
})
app.listen(PORT, () => {
    if (process.env.ENV == 'development') {
        logger.info(`Server live at http://localhost:${PORT}`);
    }
})

// Verify Token Middleware function
function verifyToken (req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== "undefined") {
        const token = bearerHeader.split(" ")[1];
        req.token = token;
        next();
    }else {
        //forbidden
        res.sendStatus(403);
    }
}
