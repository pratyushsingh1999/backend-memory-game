const mailgun = require("mailgun-js");
const logger = require('./logger');
const DOMAIN = 'sandbox3e7532c5828945daa59dd79280515d03.mailgun.org';
const API_KEY = '1270333d821fe0f0e5cbb6cccb215b1c-b6190e87-558fc1e4'
const mg = mailgun({apiKey: API_KEY, domain: DOMAIN});
const data = {
    from: 'Memory Game <memorygame@memorygame.game>',
    to: '',
    subject: '',
    text: ''
};
const sendMail = (mail , count) => {
    return new Promise((resolve, reject) => {
        data.to = mail;
        data.subject = 'New Highest score',
        data.text = 'Congratulations your new highest score is ' + count;
        mg.messages().send(data, function (error, body) {
            if (error) {
                logger.error('Mail gun error' + error);
                reject(error);
            }else {
                logger.info('Mail sent to '+ mail + " " + body);
                resolve(body)
            }
        });
    })
}
const sendVerificationMail = (mail) => {
    return new Promise((resolve, reject) => {
        data.to = mail;
        data.subject = 'Verify email';
        data.text = `<p>Click on this link to verify email</p> <a href="https://backend-memory-game.herokuapp.com/api/verifyEmail?email=${mail}">Verify email</a>`;
        mg.messages().send(data, function (error, body) {
            if (error) {
                logger.error('Mail gun error' + error);
                reject(error);
            }else {
                logger.info('Mail sent to '+ mail + " " + body);
                resolve(body)
            }
        });
    })
}
module.exports = {sendMail , sendVerificationMail};
