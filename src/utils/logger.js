const { createLogger, transports, format , addColors } = require("winston");
const path = require("path");
var logDir = path.join(__dirname , '../../' ,'logs');

const logger = createLogger({
    transports: [
        new transports.File({
            filename: path.join(logDir, "error.log"),
            level: "error",
            format: format.combine(format.colorize(),format.timestamp(), format.json()),
        }),
        new transports.File({
            filename: path.join(logDir, "info.log"),
            level: "info",
            format: format.combine(format.colorize(),format.timestamp(), format.json()),
        }),
        new transports.Console({
            format: format.combine(format.colorize())
        })
    ],
});

module.exports = logger;
