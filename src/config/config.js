const mysql = require('mysql2');
require('dotenv').config();
const logger = require('../utils/logger');

const connection = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
})
connection.query('SELECT * FROM ScoreList', (err,data)=> {
    if (err) {
        logger.info('Creating Table');
        connection.query('CREATE TABLE ScoreList ( ' +
            '  playerName text, ' +
            '  level text, ' +
            '  bestScore int ' +
            ')' , (err , data) => {
            if (err) {
                logger.error(err);
            }
        })
    } else {
        logger.info('Fetching data from pre existing table');
    }
})
connection.query('SELECT * FROM UserList', (err,data)=> {
    if (err) {
        logger.info('Creating Table UserList');
        connection.query('CREATE TABLE UserList ( ' +
            '  name text, ' +
            '  email text, ' +
            '  password text, ' +
            '  verified boolean ' +
            ')' , (err , data) => {
            if (err) {
                logger.error(err);
            }
        })
    } else {
        logger.info('Fetching data from pre existing table');
    }
})


module.exports = connection;
