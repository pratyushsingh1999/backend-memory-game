const Joi = require('joi')

const Schema = Joi.object({
    name: Joi.string().required(),
    level: Joi.string().required(),
    bestScore: Joi.number().required()
})

module.exports = Schema;
