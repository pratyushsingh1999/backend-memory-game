const Joi = require('joi')

const Schema = Joi.object({
    email: Joi.string().required().email(),
    password: Joi.string().required()
})

module.exports = Schema;
