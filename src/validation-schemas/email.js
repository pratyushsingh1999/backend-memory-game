const Joi = require('joi')

const Schema = Joi.object({
    email: Joi.string().required().email(),
    count: Joi.number().required()
})

module.exports = Schema;
